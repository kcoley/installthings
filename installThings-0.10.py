#!/usr/bin/env python
import os
import sys
#script for installing packages
packages = ['vim',
            'g++',
            'python-pip',
            'mercurial',
            'python-dev',
            'python-numpy',
            'oracle-java8-installer',
            'atom',
            'libav-tools',
            'libsdl-image1.2-dev',
            'libsdl-mixer1.2-dev',
            'libsdl-ttf2.0-dev',
            'libsmpeg-dev',
            'libsdl1.2-dev',
            'libportmidi-dev',
            'libswscale-dev',
            'libavformat-dev',
            'libavcodec-dev',
            'git',
            'grub-customizer'
            ]


repos = ['ppa:webupd8team/java',
         'ppa:webupd8team/atom',
         'ppa:danielrichter2007/grub-customizer'
        ]

pip_package_repos = ['http://bitbucket.org/pygame/pygame']

if __name__ == "__main__":
    if os.getuid() != 0:
        print("You are not root!  Activating sudo powers...")
        args = ['sudo', sys.executable] + sys.argv + [os.environ]
        os.execlpe('sudo', *args)

    if os.getuid() == 0:
        print "Adding repos..."
        for repo in repos:
            os.system('add-apt-repository ' + repo)

        os.system('apt-get update')

        print "Installing packages..."
        for package in packages:
            os.system('apt-get install -y ' + package)

        print "Installing pip packages"
        for pip_package_repo in pip_package_repos:
            os.system('pip install hg+' + pip_package_repo)

        print "Script Complete!"
    else:
        print "You are still not root.  Unable to execute script... :("
